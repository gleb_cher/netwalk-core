export default class Stopwatch {
  constructor({
    updateInterval = 1000,
    timeChangeCallback,
    statusChangeCallback,
  }) {
    this.timeChangeCallback = timeChangeCallback;
    this.statusChangeCallback = statusChangeCallback;

    this._updateInterval = updateInterval;

    this.status = 'reset'; // may be 'pause', 'on', 'reset'
    this.time = 0;

    this._interval = null;
    this._offset = null;
  }

  get status() {
    return this._status;
  }

  set status(value) {
    this._status = value;
    if (this.statusChangeCallback) this.statusChangeCallback(this.status);
  }

  get time() {
    return this._time;
  }


  set time(value) {
    this._time = value;
    if (this.timeChangeCallback) this.timeChangeCallback(this.time);
  }


  update() {
    const now = Date.now();
    this.time += now - this._offset;
    this._offset = now;
  }

  start() {
    if (this.status !== 'on') {
      this._offset = Date.now();
      this._interval = setInterval(() => {
        this.update();
      }, this._updateInterval);
      this.status = 'on';
    }
  }

  pause({ silent = false }) {
    clearInterval(this._interval);
    this._interval = null;

    if (!silent) this.status = 'pause';
  }

  reset() {
    if (this.status === 'on') this.pause({ silent: true });
    this.time = 0;
    this.status = 'reset';
  }
}
