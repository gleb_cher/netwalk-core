import _random from 'lodash/random';
import _union from 'lodash/union';
import _sampleSize from 'lodash/sampleSize';
import _sample from 'lodash/sample';
import _times from 'lodash/times';

import Vector from 'victor';

import { directions, oppositeConnections } from './constants';
import Cell from './cell';

export default class Matrix {
  constructor({
    template, rows, columns, matrixChangeCallback,
  }) {
    this.matrixChangeCallback = matrixChangeCallback;

    if (template) {
      if (Matrix.validateTemplate(template)) {
        this.matrix = Matrix.createMatrixByTemplate(template);
      } else throw new Error('invalid template');
    } else if (rows && columns) {
      this.matrix = Matrix.createMatrixByRowsAndColumns(rows, columns);
    } else throw new Error('impossible create matrix without params');
  }

  get matrix() {
    return this._matrix;
  }

  set matrix(value) {
    this._matrix = value;
    if (this.matrixChangeCallback) this.matrixChangeCallback();
  }

  async fillMatrix(delay) {
    await Matrix.fillMatrix(this.matrix, delay, this.matrixChangeCallback);
  }

  async randomizeMatrix(delay) {
    await Matrix.randomizeMatrix(this.matrix, this.matrixChangeCallback, delay);
  }

  checkMatrixIsSolved() {
    return Matrix.checkMatrixSolved(this.matrix);
  }

  rotateNode(id) {
    const node = Matrix.getCellById(this.matrix, id);
    if (!node) return false;

    node.rotateNode();
    Matrix.actualizeConnectionStatuses(this.matrix, this.matrixChangeCallback);

    return true;
  }


  static createMatrixByRowsAndColumns(rows, columns) {
    const matrix = [];
    for (let rowIndex = 0; rowIndex < rows; rowIndex++) {
      matrix.push(Array.from({ length: columns }, (v, columnIndex) => new Cell({
        type: 'simple',
        y: rowIndex,
        x: columnIndex,
      })));
    }
    return matrix;
  }

  static createMatrixByTemplate(template) {
    const matrix = [];
    template.forEach((row, rowIndex) => {
      matrix.push(row.map((cell, columnIndex) => {
        let type;
        switch (cell) {
          case 'provider':
            type = 'provider';
            break;
          case 1:
            type = 'simple';
            break;
          case 0:
            type = 'empty';
            break;
        }
        return new Cell({
          y: rowIndex,
          x: columnIndex,
          type,
        });
      }));
    });
    return matrix;
  }

  static validateTemplate(template) {
    return Array.isArray(template)
      && template.length > 0
      && templateRowsAreArrays(template)
      && hasValidLengths(template)
      && hasEqualRowsLength(template)
      && hasNoMoreThanOneProvider(template)
      && isResolvable(template);

    function hasNoMoreThanOneProvider(template) {
      let providersCounter = 0;
      for (const row of template) {
        for (const cell of row) {
          if (cell === 'provider') {
            providersCounter++;
            if (providersCounter > 1) return false;
          }
        }
      }

      return true;
    }

    function templateRowsAreArrays(template) {
      for (const row of template) {
        if (!Array.isArray(row)) return false;
      }
      return true;
    }

    function hasValidLengths(template) {
      let notEmptyCellCounter = 0;
      for (const row of template) {
        for (const cell of row) {
          if (cell !== 0) notEmptyCellCounter++;
        }
      }
      return notEmptyCellCounter > 1;
    }

    function hasEqualRowsLength(template) {
      const templateRowLength = template[0].length;
      return !template.slice(1).some(row => row.length !== templateRowLength);
    }

    function isResolvable(template) {
      const testMatrix = template.map((row, rowIndex) => row.map((cell, cellIndex) => ({
        vector: new Vector(cellIndex, rowIndex),
        type: cell ? null : 'empty',
        connectable: false,
      })));
      const allNotEmptyCells = Matrix.getAllNotEmptyCells(testMatrix);
      const randomNotEmptyCell = allNotEmptyCells[_random(0, allNotEmptyCells.length - 1)];

      markNotEmptyNeighborsRecursive(testMatrix, randomNotEmptyCell);

      function markNotEmptyNeighborsRecursive(matrix, cell) {
        const notEmptyCellNeighbors = Matrix.getNotEmptyCellNeighbors(matrix, cell);
        if (!notEmptyCellNeighbors) return;

        if (cell.connectable !== true) cell.connectable = true;

        notEmptyCellNeighbors
          .filter(cell => !cell.connectable)
          .forEach(cell => markNotEmptyNeighborsRecursive(matrix, cell));
      }

      return !testMatrix.some(row => row.some(cell => cell.type !== 'empty' && !cell.connectable));
    }
  }


  static getAllNotEmptyCells(matrix) {
    const allNotEmptyCells = [];
    for (const row of matrix) {
      for (const cell of row) {
        if (cell.type !== 'empty') allNotEmptyCells.push(cell);
      }
    }
    return allNotEmptyCells;
  }

  static getCellByType(matrix, type) {
    for (const row of matrix) {
      for (const cell of row) {
        if (cell.type === type) return cell;
      }
    }
    return null;
  }

  static getCellById(matrix, id) {
    for (const row of matrix) {
      for (const cell of row) {
        if (cell.id === id) return cell;
      }
    }
    return null;
  }

  static getCellByVector(matrix, vector) {
    if (Matrix.checkVectorIsInMatrix(matrix, vector)) {
      return matrix[vector.y][vector.x];
    } return null;
  }

  static getCellNeighbors(matrix, cell) {
    const neighbors = [];
    for (const direction of directions) {
      const neighborVector = cell.vector.clone().add(direction.vector);
      const neighborCell = Matrix.getCellByVector(matrix, neighborVector);
      if (neighborCell) neighbors.push(neighborCell);
    }
    return neighbors;
  }

  static getNotEmptyCellNeighbors(matrix, cell) {
    const neighbors = Matrix.getCellNeighbors(matrix, cell);
    return neighbors.filter(cell => cell.type !== 'empty');
  }

  static getCellNeighborsReadyToConnect(matrix, cell) {
    const neighborCells = Matrix.getCellNeighbors(matrix, cell);
    if (neighborCells.length > 0) {
      const readyCells = neighborCells.filter(cell => Matrix.cellIsReadyToConnect(cell));
      if (readyCells.length > 0) return readyCells;
    }

    return null;
  }

  static getConsumerCellsWithReadyToConnectNeighbor(matrix) {
    const allConsumerCells = Matrix.getAllCellsByType(matrix, 'consumer');
    return allConsumerCells.filter(cell => Matrix.cellHasNeighborsReadyToConnect(matrix, cell));
  }

  static getDirectionByNeighborsVectors(firstVector, secondVector) {
    // todo test
    const { x, y } = firstVector.clone().subtract(secondVector);

    if (x === 0) {
      switch (y) {
        case 1:
          return 'up';
        case -1:
          return 'down';
      }
    }
    if (y === 0) {
      switch (x) {
        case 1:
          return 'left';
        case -1:
          return 'right';
      }
    }
  }

  static getAllCellsByType(matrix, type) {
    let result = [];
    matrix.forEach((row) => {
      const cellsByTypeInRow = row.filter((cell) => {
        if (Array.isArray(type)) {
          for (const targetType of type) {
            if (cell.type === targetType) return true;
          }
          return false;
        }
        return cell.type === type;
      });
      result = [...result, ...cellsByTypeInRow];
    });

    return result.length > 0 ? result : null;
  }


  static checkVectorIsInMatrix(matrix, vector) {
    const { x, y } = vector;
    const yMax = matrix.length - 1;
    const xMax = matrix[0].length - 1;
    const yMin = 0;
    const xMin = 0;

    return (x >= xMin && x <= xMax) && (y >= yMin && y <= yMax);
  }

  static cellHasNeighborsReadyToConnect(matrix, cell) {
    const notEmptyCellNeighbors = Matrix.getNotEmptyCellNeighbors(matrix, cell);
    return notEmptyCellNeighbors.some(cell => Matrix.cellIsReadyToConnect(cell));
  }

  static cellIsReadyToConnect(cell) {
    const { type, connections } = cell;
    return type === 'simple' && connections.length === 0;
  }

  static checkCellsAreConnected(firstCell, secondCell) {
    const directionToSecondCell = Matrix.getDirectionByNeighborsVectors(firstCell.vector, secondCell.vector);
    const directionToFirstCell = oppositeConnections[directionToSecondCell];

    return firstCell.connections.includes(directionToSecondCell)
      && secondCell.connections.includes(directionToFirstCell);
  }

  static checkMatrixSolved(matrix) {
    const targetCells = Matrix.getAllCellsByType(matrix, ['consumer', 'provider']);
    return !targetCells.some(cell => cell.connected === false);
  }


  static connectCells(firstCell, secondCell) {
    // todo test
    const newFirstCellConnection = Matrix.getDirectionByNeighborsVectors(firstCell.vector, secondCell.vector);
    const newSecondCellConnection = oppositeConnections[newFirstCellConnection];

    firstCell.connections = _union(firstCell.connections, [newFirstCellConnection]);
    secondCell.connections = _union(secondCell.connections, [newSecondCellConnection]);
  }


  static createProvider(matrix, matrixChangeCallback) {
    const allNotEmptyCells = Matrix.getAllNotEmptyCells(matrix);
    const randomNotEmptyCell = allNotEmptyCells[_random(0, allNotEmptyCells.length - 1)];
    Object.assign(randomNotEmptyCell, {
      type: 'provider',
      connected: true,
      actualized: true,
    });
    if (matrixChangeCallback) matrixChangeCallback();

    return randomNotEmptyCell;
  }

  static asyncCreateInitialConsumer(matrix, providerCell, delay) {
    return new Promise((resolve) => {
      setTimeout(() => {
        const providerNeighborsReadyToConnect = Matrix.getCellNeighborsReadyToConnect(matrix, providerCell);

        if (providerNeighborsReadyToConnect) {
          const initialConsumerCell = _sample(providerNeighborsReadyToConnect);
          initialConsumerCell.type = 'consumer';
          initialConsumerCell.connected = true;
          Matrix.connectCells(providerCell, initialConsumerCell);
        }
        resolve();
      }, delay);
    });
  }

  static createInitialConsumer(matrix, providerCell) {
    const providerNeighborsReadyToConnect = Matrix.getCellNeighborsReadyToConnect(matrix, providerCell);

    if (providerNeighborsReadyToConnect) {
      const initialConsumerCell = _sample(providerNeighborsReadyToConnect);
      initialConsumerCell.type = 'consumer';
      initialConsumerCell.connected = true;
      Matrix.connectCells(providerCell, initialConsumerCell);
    }
  }


  static fillCellNeighbors(matrix, cell) {
    const cellNeighborsReadyToConnect = Matrix.getCellNeighborsReadyToConnect(matrix, cell);
    if (cellNeighborsReadyToConnect) {
      cell.type = 'simple';
      const randomCellNeighborsReadyToConnect = _sampleSize(cellNeighborsReadyToConnect, 2);
      for (const neighborCell of randomCellNeighborsReadyToConnect) {
        neighborCell.type = 'consumer';
        Matrix.connectCells(cell, neighborCell);
      }

      return randomCellNeighborsReadyToConnect;
    }
    return null;
  }


  static recursiveFillConsumerCellsNeighbors(matrix) {
    const consumerCellsWithReadyToConnectNeighbor = Matrix.getConsumerCellsWithReadyToConnectNeighbor(matrix);
    if (consumerCellsWithReadyToConnectNeighbor.length > 0) {
      Matrix.fillCellNeighbors(matrix, _sample(consumerCellsWithReadyToConnectNeighbor));
      Matrix.recursiveFillConsumerCellsNeighbors(matrix);
    }
  }

  static asyncRecursiveFillConsumerCellsNeighbors(matrix, delay, matrixChangeCallback) {
    return new Promise((resolve) => {
      setTimeout(async () => {
        const consumerCellsWithReadyToConnectNeighbor = Matrix.getConsumerCellsWithReadyToConnectNeighbor(matrix);
        if (consumerCellsWithReadyToConnectNeighbor.length > 0) {
          Matrix.fillCellNeighbors(matrix, _sample(consumerCellsWithReadyToConnectNeighbor));
          Matrix.actualizeConnectionStatuses(matrix, matrixChangeCallback);
        }

        if (Matrix.getConsumerCellsWithReadyToConnectNeighbor(matrix).length > 0) {
          await Matrix.asyncRecursiveFillConsumerCellsNeighbors(matrix, delay, matrixChangeCallback);
        }
        resolve();
      }, delay);
    });
  }


  static async asyncCreateInitialConsumerAndRecursivelyFillHisNeighbors(matrix, providerCell, delay, matrixChangeCallback) {
    await Matrix.asyncCreateInitialConsumer(matrix, providerCell, delay);
    await Matrix.asyncRecursiveFillConsumerCellsNeighbors(matrix, delay, matrixChangeCallback);
  }

  static createInitialConsumerAndRecursivelyFillHisNeighbors(matrix, providerCell) {
    Matrix.createInitialConsumer(matrix, providerCell);
    Matrix.recursiveFillConsumerCellsNeighbors(matrix);
  }

  static async fillMatrix(matrix, delay, matrixChangeCallback) {
    const providerCell = Matrix.getCellByType(matrix, 'provider') || Matrix.createProvider(matrix, matrixChangeCallback);

    while (Matrix.cellHasNeighborsReadyToConnect(matrix, providerCell)) {
      if (delay) {
        await Matrix.asyncCreateInitialConsumerAndRecursivelyFillHisNeighbors(matrix, providerCell, delay, matrixChangeCallback);
      } else {
        Matrix.createInitialConsumerAndRecursivelyFillHisNeighbors(matrix, providerCell);
        Matrix.actualizeConnectionStatuses(matrix, matrixChangeCallback);
      }
    }
    //  todo поиск empty и заполнение
    // Matrix.actualizeConnectionStatuses(matrix)
  }


  static markConnectedCellsRecursively(matrix, baseCell) {
    const baseCellNotEmptyNeighbors = Matrix.getNotEmptyCellNeighbors(matrix, baseCell);
    const targetCells = baseCellNotEmptyNeighbors.filter(targetCell => {
      return !targetCell.actualized && Matrix.checkCellsAreConnected(targetCell, baseCell);
    });
    for (const cell of targetCells) {
      Object.assign(cell, {
        connected: true,
        actualized: true,
      });
      Matrix.markConnectedCellsRecursively(matrix, cell);
    }
  }

  static actualizeConnectionStatuses(matrix, matrixChangeCallback) {
    const allNotEmptyCells = Matrix.getAllNotEmptyCells(matrix);
    for (const cell of allNotEmptyCells) {
      if (cell.type !== 'provider') cell.actualized = false;
    }
    Matrix.markConnectedCellsRecursively(matrix, Matrix.getCellByType(matrix, 'provider'));

    for (const cell of allNotEmptyCells) {
      if (!cell.actualized) {
        Object.assign(cell, { actualized: true, connected: false });
      }
    }
    if (matrixChangeCallback) matrixChangeCallback();
  }

  static randomizeCell(cell) {
    _times(_random(1, 3), () => cell.rotateNode());
    Object.assign(cell, {
      randomized: false,
    });
  }

  static asyncRandomizeRow(row, delay) {
    return new Promise((resolve) => {
      setTimeout(() => {
        for (const cell of row) {
          if (cell.type !== 'empty') Matrix.randomizeCell(cell);
        }
        resolve();
      }, delay);
    });
  }

  static async randomizeMatrix(matrix, matrixChangeCallback, delay) {
    if (delay) {
      for (const row of matrix) {
        await Matrix.asyncRandomizeRow(row, delay);
        Matrix.actualizeConnectionStatuses(matrix, matrixChangeCallback);
      }
    } else {
      const notEmptyCells = Matrix.getAllNotEmptyCells(matrix);
      notEmptyCells.forEach((cell) => { Matrix.randomizeCell(cell); });
      Matrix.actualizeConnectionStatuses(matrix, matrixChangeCallback);
    }
  }
}
