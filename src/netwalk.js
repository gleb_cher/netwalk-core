import Matrix from './matrix';
import Stopwatch from './utils/stopwatch';

export default class Netwalk {
  constructor({
    template = null,
    rows = null,
    columns = null,

    matrixChangeCallback = null,
    stopwatchChangeCallback = null,
    movesChangeCallback = null,
    startedChangeCallback = null,
    finishedChangeCallback = null,
    readyChangeCallback = null,
    matrixFillingChangeCallback = null,
    matrixFilledChangeCallback = null,
    matrixRandomizingChangeCallback = null,
    matrixRandomizedChangeCallback = null,

    fillAnimationDelay = 0,
    randomizeAnimationDelay = 0,

    fill = true,
    randomize = true,
  }) {
    this.stopwatchInstance = new Stopwatch({ timeChangeCallback: (value) => { this.stopwatch = value; } });

    this.template = template;
    this.rows = rows;
    this.columns = columns;

    this.fillAnimationDelay = fillAnimationDelay;
    this.randomizeAnimationDelay = randomizeAnimationDelay;

    this.matrixChangeCallback = matrixChangeCallback;
    this.stopwatchChangeCallback = stopwatchChangeCallback;
    this.movesChangeCallback = movesChangeCallback;
    this.startedChangeCallback = startedChangeCallback;
    this.finishedChangeCallback = finishedChangeCallback;
    this.readyChangeCallback = readyChangeCallback;
    this.matrixRandomizedChangeCallback = matrixRandomizedChangeCallback;
    this.matrixFilledChangeCallback = matrixFilledChangeCallback;
    this.matrixFillingChangeCallback = matrixFillingChangeCallback;
    this.matrixRandomizingChangeCallback = matrixRandomizingChangeCallback;

    this.setInitialStatuses();

    this.matrixInstance = new Matrix({
      template: this.template,
      rows: this.rows,
      columns: this.columns,
      matrixChangeCallback: this.matrixChangeCallbackMiddleware(this.matrixChangeCallback),
    });

    this.prepare({ fill, randomize });
  }

  setInitialStatuses() {
    this.moves = 0;
    this.ready = false;

    this.started = false;
    this.finished = false;

    this.matrixFilling = false;
    this.matrixFilled = false;

    this.matrixRandomizing = false;
    this.matrixRandomized = false;
  }

  async prepare({ fill, randomize }) {
    if (fill) {
      await this.fillMatrix();
      if (randomize) {
        await this.randomizeMatrix();
      }
    }
  }

  get matrix() {
    return this._matrixInstance.matrix;
  }

  set matrix(value) {
    this._matrixInstance.matrix = value;
  }


  get matrixInstance() {
    return this._matrixInstance;
  }

  set matrixInstance(value) {
    this._matrixInstance = value;
  }


  get stopwatch() {
    return this._stopwatch;
  }

  set stopwatch(value) {
    this._stopwatch = value;
    if (this.stopwatchChangeCallback) this.stopwatchChangeCallback(value);
  }


  get moves() {
    return this._moves;
  }

  set moves(value) {
    this._moves = value;
    if (!this.started && this.ready) this.started = true;
    if (this.movesChangeCallback) this.movesChangeCallback(value);
  }


  get started() {
    return this._started;
  }

  set started(value) {
    this._started = value;
    if (value === true) {
      if (this.stopwatchInstance && this.stopwatchInstance.status !== 'on') this.stopwatchInstance.start();
    }
    if (this.startedChangeCallback) this.startedChangeCallback(value);
  }


  get ready() {
    return this._ready;
  }

  set ready(value) {
    this._ready = value;
    if (this.readyChangeCallback) this.readyChangeCallback(value);
  }


  get finished() {
    return this._finished;
  }

  set finished(value) {
    this._finished = value;
    this.stopwatchInstance.pause({});
    if (this.finishedChangeCallback) this.finishedChangeCallback(value);
  }


  get matrixFilling() {
    return this._matrixFilling;
  }

  set matrixFilling(value) {
    this._matrixFilling = value;
    if (this.matrixFillingChangeCallback) this.matrixFillingChangeCallback(value);
  }


  get matrixFilled() {
    return this._matrixRandomized;
  }

  set matrixFilled(value) {
    this._matrixFilled = value;
    if (this.matrixFilledChangeCallback) this.matrixFilledChangeCallback(value);
    this.actualizeReady();
  }


  get matrixRandomizing() {
    return this._matrixRandomizing;
  }

  set matrixRandomizing(value) {
    this._matrixRandomizing = value;
    if (this.matrixRandomizingChangeCallback) this.matrixRandomizingChangeCallback(value);
  }


  get matrixRandomized() {
    return this._matrixRandomized;
  }

  set matrixRandomized(value) {
    this._matrixRandomized = value;
    if (this.matrixRandomizedChangeCallback) this.matrixRandomizedChangeCallback(value);
    this.actualizeReady();
  }


  async fillMatrix() {
    if (!this.matrixFilled) {
      this.matrixFilling = true;

      await this.matrixInstance.fillMatrix(this.fillAnimationDelay);

      this.matrixFilling = false;
      this.matrixFilled = true;
    }
  }

  rotateNode(id) {
    if (this.ready && !this.finished) {
      this.moves++;
    }
    this.matrixInstance.rotateNode(id);
  }

  async randomizeMatrix() {
    if (!this.matrixRandomized) {
      this.matrixRandomizing = true;

      await this.matrixInstance.randomizeMatrix(this.randomizeAnimationDelay);

      this.matrixRandomizing = false;
      this.matrixRandomized = true;
    }
  }

  // stopMatrixFilling() {
  //   this.matrixInstance.stopFilling();
  //   this.matrixFilling = false;
  // }

  // stopMatrixRandomizing() {
  //   this.matrixInstance.stopRandomizing();
  //   this.matrixRandomizing = false;
  // }

  destroy() {
    // if (this.matrixFilling) this.stopMatrixFilling();
    // if (this.matrixRandomizing) this.stopMatrixRandomizing();
    if (this.stopwatchInstance) this.stopwatchInstance.reset();
  }

  // reset({ fill = true, randomize = true }) {
  //   this.stop();
  //   this.setInitialStatuses();
  //
  //   this.initialize({ fill, randomize });
  // }

  matrixChangeCallbackMiddleware(callback) {
    return () => {
      if (this.started && this.matrixInstance.checkMatrixIsSolved()) {
        this.finished = true;
      }
      if (callback) callback();
    };
  }

  actualizeReady() {
    if (this.matrixFilled && this.matrixRandomized) {
      this.ready = true;
    }
  }
}
