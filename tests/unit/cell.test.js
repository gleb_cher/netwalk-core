import Cell from '../../src/cell';
import { connectionTypes } from '../../src/constants';

describe('Cell class', () => {
  describe('initialize', () => {
    test('must throw Error then x and y are undefined', () => {
      function createCell() {
        return new Cell({});
      }
      expect(createCell).toThrowError();
    });
  });

  describe('get connection params', () => {
    test('must return null if such template is not exist', () => {
      expect(Cell.getConnectionParamsByConnections([])).toBe(null);
    });
    test('must return "elbow" template in 2 position', () => {
      expect(Cell.getConnectionParamsByConnections(connectionTypes.elbow[2])).toEqual({ position: 2, type: 'elbow' });
    });
  });
});
