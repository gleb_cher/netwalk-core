import Vector from 'victor';
import Matrix from '../../src/matrix';
import Cell from '../../src/cell';

const S = 'provider';

describe('Matrix', () => {
  describe('initialization', () => {
    test('must throw Error with no params', () => {
      expect(() => new Matrix({})).toThrowError();
    });

    test('must validate template', () => {
      expect(() => new Matrix({ template: 'wrong template' })).toThrowError();
    });
  });

  describe('template validation', () => {
    test('must be two-dimensional array', () => {
      const goodTemplates = [
        [
          [1, 1, 0],
          [1, S, 0],
        ],
        [[1, 1]],
      ];
      const badTemplates = [
        [],
        'notArray',
        ['notArray'],
        [[], 'notArray'],
      ];

      for (const template of badTemplates) {
        expect(Matrix.validateTemplate(template)).toBe(false);
      }

      for (const template of goodTemplates) {
        expect(Matrix.validateTemplate(template)).toBe(true);
      }
    });
    test('must have > 1 not empty cells', () => {
      expect(Matrix.validateTemplate([[1]])).toBe(false);
      expect(Matrix.validateTemplate([[1, 0]])).toBe(false);
    });
    test('must have not more than 1 provider', () => {
      expect(Matrix.validateTemplate([[1, S], [0, S]])).toBe(false);
      expect(Matrix.validateTemplate([[1, S], [1, 0]])).toBe(true);
    });
    test('must have equal row lengths', () => {
      expect(Matrix.validateTemplate([[1, 1], [1]])).toBe(false);
    });
    test('must be resolvable', () => {
      expect(Matrix.validateTemplate(
        [
          [0, 0, 1],
          [1, 0, 0],
        ],
      )).toBe(false);
    });
  });

  describe('matrix creation', () => {
    expect.extend({
      toBeTwoDimensionalArray(received) {
        const pass = Array.isArray(received) && !received.some(row => !Array.isArray(row));
        return {
          message: () => `output is ${pass ? '' : 'not'} two-dimensional array`,
          pass,
        };
      },
      toConsistOfCells(received) {
        const pass = !received.some(row => row.some(cell => !(cell instanceof Cell)));
        return {
          message: () => `output is ${pass ? '' : 'not'} consist of cells`,
          pass,
        };
      },
      toHaveEmptyCell(received) {
        const pass = received.some(row => row.some(cell => cell.type === 'empty'));
        return {
          message: () => `output have ${pass ? '' : 'not'} empty cell`,
          pass,
        };
      },
      toHaveProviderCell(received) {
        const pass = received.some(row => row.some(cell => cell.type === 'provider'));
        return {
          message: () => `output have ${pass ? '' : 'not'} empty cell`,
          pass,
        };
      },
    });
    describe('matrix creation by template', () => {
      test('must be two-dimensional array', () => {
        expect(Matrix.createMatrixByTemplate([[0, 0], [1, 1]])).toBeTwoDimensionalArray();
      });
      test('must consist of Cells', () => {
        expect(Matrix.createMatrixByTemplate([[1, 1], [0, 0]])).toConsistOfCells();
      });
      test('must have empty cell', () => {
        expect(Matrix.createMatrixByTemplate([[1, 1], [0, 1]])).toHaveEmptyCell();
      });
      test('must have provider cell', () => {
        expect(Matrix.createMatrixByTemplate([[1, 1], ['provider', 1]])).toHaveProviderCell();
      });
    });

    describe('matrix creation by rows and columns', () => {
      test('must be two-dimensional array', () => {
        expect(Matrix.createMatrixByRowsAndColumns(2, 2)).toBeTwoDimensionalArray();
      });
      test('must consist of Cells', () => {
        expect(Matrix.createMatrixByRowsAndColumns(2, 2)).toConsistOfCells();
      });
    });
  });

  describe('work with cells', () => {
    describe('get cell by vector', () => {
      const testMatrix = Matrix.createMatrixByRowsAndColumns(2, 2);
      test('must return null if the cell is not', () => {
        expect(Matrix.getCellByVector(testMatrix, new Vector(3, 3))).toBe(null);
      });
    });
    describe('get cell by type', () => {
      test('must return provider cell', () => {
        const testMatrix = Matrix.createMatrixByTemplate([[1, 1, 'provider']]);
        expect(Matrix.getCellByType(testMatrix, 'provider')).toMatchObject({ type: 'provider' });
      });
      test('must return null', () => {
        const testMatrix = Matrix.createMatrixByTemplate([[1, 1, 0]]);
        expect(Matrix.getCellByType(testMatrix, 'provider')).toBe(null);
      });
    });
    describe('get all not empty cells', () => {
      expect.extend({
        toBeArrayWhereAllCellsAreNotEmpty(received) {
          const pass = !received.some(cell => cell.type === 'empty');
          return {
            message: () => `${pass ? '' : 'not'} all cells are not empty`,
            pass,
          };
        },
      });

      test('all cells are not empty', () => {
        const testMatrix = Matrix.createMatrixByTemplate([[1, 'provider', 1, 0], [0, 1, 0, 1]]);
        expect(Matrix.getAllNotEmptyCells(testMatrix)).toBeArrayWhereAllCellsAreNotEmpty();
      });
    });
    describe('check vector is in matrix', () => {
      const testMatrix = Matrix.createMatrixByRowsAndColumns(2, 2);

      test('must return false then vector is not in matrix', () => {
        expect(Matrix.checkVectorIsInMatrix(testMatrix, Vector(3, 3))).toBe(false);
      });
      test('must return true then vector is in matrix', () => {
        expect(Matrix.checkVectorIsInMatrix(testMatrix, Vector(1, 1))).toBe(true);
      });
    });
    describe('get cell neighbors', () => {
      test('must return array with length = 1', () => {
        const testMatrix = Matrix.createMatrixByTemplate([[1, 1]]);
        expect(Matrix.getCellNeighbors(testMatrix, testMatrix[0][0])).toHaveLength(1);
      });
      test('must return array with length = 2', () => {
        const testMatrix = Matrix.createMatrixByTemplate([[1, 1], [1, 1]]);

        expect(Matrix.getCellNeighbors(testMatrix, testMatrix[0][0])).toHaveLength(2);
      });
      test('must return array with length = 4', () => {
        const testMatrix = Matrix.createMatrixByTemplate([
          [1, 1, 1],
          [1, 1, 1],
          [1, 1, 1],
        ]);
        expect(Matrix.getCellNeighbors(testMatrix, testMatrix[1][1])).toHaveLength(4);
      });
    });
    describe('create provider cell', () => {
      test('must return provider cell', () => {
        const testMatrix = Matrix.createMatrixByRowsAndColumns(2, 2);
        expect(Matrix.createProvider(testMatrix)).toMatchObject({ type: 'provider' });
      });
    });
    describe('check cells are connected', () => {
      const firstCell = new Cell({ x: 0, y: 0, connections: ['down'] });
      test('must return true', () => {
        const secondCell = new Cell({ x: 0, y: 1, connections: ['up'] });
        expect(Matrix.checkCellsAreConnected(firstCell, secondCell)).toBe(true);
      });
      test('must return false', () => {
        const secondCell = new Cell({ x: 0, y: 1, connections: ['left'] });
        expect(Matrix.checkCellsAreConnected(firstCell, secondCell)).toBe(false);
      });
    });
  });
});
